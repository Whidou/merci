<?php
/**
*
* Merci extension for the phpBB Forum Software package.
* French translation by Galixte (http://www.galixte.com)
*
* @copyright (c) 2015 rxu
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ « » “ ” …
//

$lang = array_merge($lang, array(
	'ACP_POSTS'							=> 'Nombre de messages',
	'ACP_POSTSEND'						=> 'Nombre de messages remerciés restants à prendre en compte',
	'ACP_USERSEND'						=> 'Nombre d’utilisateurs ayant remercié restants à prendre en compte',

	'GRAPHIC_BLOCK_BACK'				=> 'ext/fmr/Merci/images/rating/reput_block_back.gif',
	'GRAPHIC_BLOCK_RED'					=> 'ext/fmr/Merci/images/rating/reput_block_red.gif',
	'GRAPHIC_DEFAULT'					=> 'Images',
	'GRAPHIC_OPTIONS'					=> 'Options graphiques',
	'GRAPHIC_STAR_BACK'					=> 'ext/fmr/Merci/images/rating/reput_star_back.gif',
	'GRAPHIC_STAR_BLUE'					=> 'ext/fmr/Merci/images/rating/reput_star_blue.gif',
	'GRAPHIC_STAR_GOLD'					=> 'ext/fmr/Merci/images/rating/reput_star_gold.gif',



	'REFRESH'							=> 'Envoyer',

	'STEPR'								=> ' - exécuté(s), étape %s',

	'TRUNCATE'							=> 'Envoyer',
));
