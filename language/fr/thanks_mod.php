<?php
/**
*
* Merci extension for the phpBB Forum Software package.
* French translation by Galixte (http://www.galixte.com)
*
* @copyright (c) 2015 rxu
* @copyright (c) 2017 Whidou
*
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ « » “ ” …
//

$lang = array_merge($lang, array(

	'DISABLE_REMOVE_THANKS'		=> 'La suppression des remerciements a été désactivée par l’administrateur.',

	'GLOBAL_INCORRECT_THANKS'	=> 'Il n’est pas autoriser de remercier une annonce globale qui n’est pas rattachée à un forum en particulier.',

	'INCORRECT_THANKS'			=> 'Ce remerciement est invalide.',

	'JUMP_TO_FORUM'				=> 'Aller au forum',
	'JUMP_TO_TOPIC'				=> 'Aller au sujet',

	'FOR_MESSAGE'				=> ' pour le message',
	'FURTHER_THANKS'     	    => ' et un autre utilisateur',
	'FURTHER_THANKS_PL'         => ' et %d autres utilisateurs',


	'NOTIFICATION_THANKS_GIVE'	=> array(
		1 => '<strong>Remerciement reçu</strong> de %1$s pour le message&nbsp;:',
		2 => '<strong>Remerciements reçus</strong> de %1$s pour le message&nbsp;:',
	),
	'NOTIFICATION_TYPE_THANKS_GIVE'		=> 'Quelqu’un vous a remercié pour votre message.',

	'REMOVE_THANKS'				=> 'Supprimer son remerciement pour l’auteur de ce message : ',
	'REMOVE_THANKS_CONFIRM'		=> 'Confirmer la suppression de son remerciement pour l’auteur de ce message.',
	'REMOVE_THANKS_SHORT'		=> 'Supprimer son remerciement',
//	'RETURN_POST'				=> 'Retour',

	'THANK'						=> 'Heure',
	'THANK_FROM'				=> 'de',
	'THANK_TEXT_1'				=> 'Ces utilisateurs ont remercié ',
	'THANK_TEXT_2'				=> ' pour son message&nbsp;: ',
	'THANK_TEXT_2PL'			=> ' pour son message (%d au total)&nbsp;: ',
	'THANK_POST'				=> 'Remercier l’auteur de ce message&nbsp;: ',
	'THANK_POST_SHORT'			=> 'Remercier',
	'THANKS'					=> array(
		1	=> '%d fois',
		2	=> '%d fois',
	),
	'THANKS_BACK'				=> 'Retour',
	'THANKS_INFO_GIVE'			=> 'Vous venez de remercier l’auteur de ce message.',
	'THANKS_INFO_REMOVE'		=> 'Vous venez de supprimer votre remerciement pour l’auteur de ce message.',
	'THANKS_PM_MES_GIVE'		=> 'vous a remercié pour le message',
	'THANKS_PM_SUBJECT_GIVE'	=> 'Remerciement pour le message',

));
